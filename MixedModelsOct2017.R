# Mixed Models

library(lme4)
library(mlmRev)

mm1<-lmer(normexam~standLRT+sex+schgend+(1|school),Exam)
summary(mm1)

mm2<-lmer(normexam~standLRT+sex+schgend+(1+standLRT|school),Exam)
summary(mm2)

ranef(mm2)

# shrinkage example

nrandoms<-50
randomeffects<-rnorm(nrandoms,sd=.3)
obsperindividual<-rgeom(nrandoms,0.3)+1 #
randomeffectsperobservation<-rep(randomeffects,obsperindividual)
levels<-rep(c(1:nrandoms),obsperindividual)

obs<- randomeffectsperobservation+
rnorm(length(randomeffectsperobservation))

shrinkexample<-lmer(obs~(1|levels))

qqnorm(randomeffects)
qqnorm(ranef(shrinkexample)$levels[[1]])
plot(ranef(shrinkexample)$levels[[1]]~obsperindividual[obsperindividual>0],xlab="N obs for this random effect",ylab="Estimated random effect")


anova(mm2,mm1) # test for a single random effect

# using a simulation approach

h0<-mm1 # Null hypothesis with one random effect
hA<-lmer(normexam~standLRT+sex+schgend+(1|school)+(0+standLRT|school),Exam) #  alternative with two random effects, the function can only test one effect at a time
# Null hypothesis
mmeff<- lmer(normexam~standLRT+sex+schgend+(0+standLRT|school),Exam) # model containing only the random effect that will be removed
library(RLRsim)
exactRLRT(mmeff,hA,h0)

# test for a fixed effect

mm3<-lmer(normexam~standLRT+sex+schgend+(1+standLRT|school),Exam,REML=FALSE)
mm4<-lmer(normexam~standLRT+sex+(1+standLRT|school),Exam,REML=FALSE)

# chisq test

anova(mm4,mm3)

# Kenward Roger approximation

library(pkrbtest)
KRmodcomp(mm3,mm4)

# parametric boostrap, fixed effects


PBmodcomp(mm3,mm4)

# generalized linear mixed models

data(Contraception)

binmm<-glmer(use~age+urban+livch+(urban|district), family=binomial,data=Contraception, nAGQ = 1L)

summary(binmm)

# cattle bovine pleuropneumonia data

data(cbpps)
summary(cbpp)
help(cbpp)
m1 <- glmer(cbind(incidence, size - incidence) ~ period + (1 | herd),
             family = binomial, data = cbpp) 


# accomodation overdispersion

obs<-gl(length(cbpp$incidence),1) # group list
mq1 <- glmer(cbind(incidence, size - incidence) ~ period + (1 | herd) +(1|obs), family = binomial, data = cbpp) 


